using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GCTHREEPROne
{
    public class Input : MonoBehaviour
    {
        public Rigidbody2D theRB;

        public float moveSpeed, jumpForce;

        public Transform groundPoint;
        public LayerMask whatIsGround;

        private bool isGrounded;

        private float inputX;


        // Update is called once per frame
        void Update()
        {
            theRB.velocity = new Vector2(inputX * moveSpeed, theRB.velocity.y);
            isGrounded = Physics2D.OverlapCircle(groundPoint.position, .2f, whatIsGround);

            if (theRB.velocity.x > 0f)
            {
                transform.localScale = Vector3.one;
            }
            else if (theRB.velocity.x < 0f)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }

        public void Movement(InputAction.CallbackContext context)
        {
            inputX = context.ReadValue<Vector2>().x;
        }

        public void Jump(InputAction.CallbackContext context)
        {
           if(context.performed && isGrounded)
            {
                theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
            }


        }
    }
}
