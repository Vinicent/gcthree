using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GCTHREEPROne
{
    public class CinemaschineState : MonoBehaviour
    {
        [SerializeField]
        private InputAction action;

        private Animator animator;

        private bool followCamera = true;
      

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            action.Enable();
        }

        private void OnDisable()
        {
            action.Disable();
        }

        void Start()
        {
            action.performed += _ => SwitchState();
        }

        private void SwitchState()
        {
            if(followCamera)
            {
                animator.Play("KeyCamera");
            }

            else
            {
                animator.Play("FollowCamera");
            }

            followCamera = !followCamera;
        }
    }
}
